% LLNCS package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\newcommand{\chooserandom}{\xleftarrow{\$}} % when an element is chosen randomly; like "x chosen randomly out of X"
\newcommand{\role}{\textit} % the role of an agent; like "verifier"
\newcommand{\theoremcorewidth}{\textwidth - \parindent - \parindent}

% math symbols & including bold ones
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm}

% links & clever references
\usepackage{hyperref}
\usepackage{cleveref}

% crypto-specific packages
\usepackage{cryptocode}
\usepackage{nicodemus}
\usepackage{famoser}

\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
\renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}
%
\title{Pairing-Based Identification Scheme with Short Secrets\thanks{Supported by organization x.}}
%
%\titlerunning{Abbreviated paper title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Florian Moser\inst{1}\orcidID{0000-0003-2268-2367} \and
Rolf Haenni\inst{2}\orcidID{0000-0002-6146-4074} \and
Philipp Locher\inst{2}}
%
\authorrunning{F. Moser et al.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Eidgenössische Technische Hochschule Zürich, Zürich, Switzerland
\email{moserfl@ethz.ch}
\and
Bern University of Applied Sciences, Biel, Switzerland\\
\email{\{rolf.haenni,philipp.locher\}@bfh.ch}}
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
The abstract should briefly summarize the contents of the paper in
150--250 words.

\keywords{Pairings \and Identification \and Authentication \and CHVote.}
\end{abstract}
%
%
%
\section{Introduction}
\subsection{CHVote}
OpenCHVote is developed in Biel \cite{haenni2017chvote}.

\subsection{Contributions}

\subsection{Discussion}

\section{Definitions}

We clarify notation, terminology and cryptographic primitives we use throughout this work.

\paragraph{Notation} We use upper-case letters for sets and lower-case letters for their elements (like $X = \{x_1, x_2, ..., x_n\}$). $|X|$ denotes the cardinality of the set $X$.

For algorithms, we use bold letters (like \textbf{Sign}). We denote $\chooserandom$ when we choose an element uniform at random (like $x \chooserandom X$). We forgo modeling the source of randomness explicitly; we simply assume each algorithm has access to randomness of suitable size.

\paragraph{Groups} A (multiplicative) group $\Gamma = (G, *, ^{-1}, 1)$ is an algebraic structure consisting of a set $G$ of elements, the binary operation $*: G \times G \rightarrow G$, the unary operation $^{-1}: G \rightarrow G$, and the neutral element $1$. For any $\{x, y, z\} \subset G$, the following holds: Associativity (($x * y) * z = x * (y * z)$), identity element ($1*x = x*1 = x$), and inverse element ($x* x^{-1} = 1$). $x^k$ applies the group operator $k-1$ times to $x$. An element $g \in G$ is called a generator of $G$, iff $\{g^1, ..., g^{p}\} = G$ for $p = |G|$. If the group is of prime order, every element is a generator, except the neutral element $1$. After this paragraph, our notation will refer to $\Gamma$ using $G$.

With $\mathbb{Z}_p^*$ we denote the multiplicative group of integers in which multiplications are computed modulo the prime $p$. With $\mathbb{Z}_p$ we denote the additive group of integers in which additions are computed modulo $p$. We handle negative values as $-k*x = k*(-x) = -(k*x)$ and $x^{-k} = (x^{-1})^k = (x^k)^{-1}$.

For $p$ prime, we can define the prime-order field $(\mathbb{Z}_p, +, *, -, ^{-1}, 0, 1)$ combining the additive group ($\mathbb{Z}_p, +, -, 0)$ and the multiplicative group ($\mathbb{Z}_p^*, *, ^{-1}, 1$) into a single algebraic structure with the additional property of distributivity of multiplication over addition ($(x+y)*z = (x*z) + (y*z)$ for any $\{x, y, z\} \subset \mathbb{Z}_p$).

\paragraph{Pairing} A map $\theta : X \times Y \rightarrow Z$ is called a pairing if it provides

\begin{itemize}
    \item \textit{bilinearity} ($\theta(x_1 * x_2, y) = \theta(x_1, y) * \theta(x_2, y)$ and $\theta(x, y_1 * y_2) = \theta(x, y_1) * \theta(x, y_2)$),
    \item \textit{non-degeneracy} (for all generators $x$ and $y$, $\theta(x,y)$ generates $Z$) and
    \item \textit{efficiency} ($\theta$ is efficiently computable).
\end{itemize}

We call a pairing \textit{Type 1} if $G_1 = G_2$, \textit{Type 2} if $G_1 \neq G_2$ but there exists a homomorphism from $G_2$ to $G_1$, and \textit{Type 3} if $G_1 \neq G_2$ and there exists no homomorphism. \cite{galbraith2008pairings}. Implementation of pairings are feasible using the Tate or the Weil pairing, applying Miller's algorithm \cite{lynn2007implementation}.

\subsection{Cryptographic basics}

\paragraph{Negligible}
A function $\epsilon : N \rightarrow [0, 1]$ is \textit{negligible} if for all $c \geq 0$ there exists $k_c \geq 0$ such that $\epsilon(k) \leq \frac{1}{k^c}$ for all $k > k_c$ \cite{katz2010digital}.  We declare a cryptographic scheme as \textit{secure} if the success probability of the attacker to reach its goal using its assigned capabilities is negligible.

\paragraph{Security parameter} A \textit{security parameter} describes the cryptographic security of a scheme; the amount of computational power required to break a scheme or property by a polynomially bounded adversary \cite{katz2010digital}. We denote the security parameter as $1^k$.

\paragraph{Hardness Assumptions}
The Discrete Log (DL) assumption states, that it is hard to find $x$ for given $y = g^x$. The computational Diffie-Hellman (CDH) assumption states, that it is hard to compute $g^{ab}$ from given $y = g^a$ and $z = g^b$. Further, the decisional Diffie-Hellman (DDH) states it is hard to differentiate ($g^a$, $g^b$, $g^{ab}$) and ($g^a$, $g^b$, $g^c$). \cite{katz2010digital} One can easily verify that solving DL implies solving CDH, and solving CDH implies solving DDH (while the reverse does not hold). It is assumed that DDH (and hence CDH and DL) holds in $G \subset \mathbb{Z}_p^*$ for $|G|$ prime \cite{haenni2017chvote}.

\paragraph{Solving the discrete log}
Some algorithms solving the discrete log exist which are faster than simply bruteforcing. For $p$ the prime group order, the best algorithms available are variants of the deterministic baby-step giant-step algorithm (succeeds in $O(\sqrt{p})$ time and space) or the probabilistic Pollard's rho (low space, $O(\sqrt{p})$ time) \cite{galbraith2017computing}.

\subsection{Sigma Protocols and Properties}

\paragraph{Effective relation} An \textit{effective relation} is a binary relation $R \subseteq X \times Y$, where $X, Y$ and $R$ are efficiently recognizable finite sets. Elements of $Y$ are called statements. If $(x, y) \in R$, then $x$ is called a \textit{witness} for $y$ \cite{boneh2017graduate}.

\paragraph{Sigma Protocol}
Let $R \subseteq X \times Y$ be an effective relation. A \textit{Sigma Protocol} for $R$ is a pair $(P, V)$:
\begin{itemize}
    \item $P$ is an interactive protocol algorithm called the \textit{Prover}, which takes as input the witness statement pair $(x, y) \in R$.
    \item $V$ is an interactive protocol algorithm called the \textit{Verifier}, which takes as input a statement $y \in Y$, and which outputs \texttt{accept} or \texttt{reject}.
    \begin{itemize}
        \item To start the protocol, $P$ computes a message $t$, called the \textit{commitment}, and sends $t$ to $V$;
        \item Upon receiving $P$'s commitment $t$, $V$ chooses a \textit{challenge} $c$ at random from a finite challenge space $C$, and sends $c$ to $P$;
        \item Upon receiving $V$'s challenge $c$, $P$ computes a \textit{response} $s$, and sends $s$ to $V$;
        \item Upon receiving $P$'s response $s$, $V$ outputs either \textit{accept} or \textit{reject}, which must be computed strictly as a function of the statement $y$ and the \textit{conversation} $(t, c, s)$. In particular, $V$ does not make any random choices other than the selection of the challenge - all other computations are completely deterministic.
    \end{itemize}
\end{itemize}

We require that for all $(x,y) \in R$, when $P(x, y)$ and $V(y)$ interact with each other, $V(y)$ always outputs \textit{accept} \cite{boneh2017graduate}.

\paragraph{Knowledge soundness} Let $(P,V)$ be a Sigma protocol for $R \subseteq X \times Y$. We say that $(P,V)$ provides knowledge soundness if there is an efficient deterministic algorithm \textbf{Ext} (called a \textit{witness extractor}) with the following property: Given as input a statement $y \in Y$, along with two accepting conversations $(t,c,s)$ and $(t,c',s')$ for $y$, where $c \neq c'$, algorithm \textbf{Ext} always outputs $x \in X$ such that $(x,y) \in R$ (i.e., $x$ is a witness for $y$). \cite{boneh2017graduate}

\paragraph{Honest Verifier Zero Knowledge} Let $(P,V)$ be a Sigma protocol for $R \subseteq X \times Y$ with challenge space $C$. We say that $(P,V)$ is \textit{honest verifier zero knowledge} (HVZK), if there exists an efficient probabilistic algorithm \textbf{Sim} (called a simulator) that takes as input $y \in Y$. It satisfies that for all $(x,y) \in R$, if we compute $(t, c, s) \chooserandom \textbf{Sim}(y)$, then $(t,c,s)$ has the same distribution as that of a transcript of a conversation between $P(x,y)$ and $V(y)$ \cite{boneh2017graduate}.

\paragraph{Special Honest Verifier Zero Knowledge}
Let $(P,V)$ be a Sigma protocol for $R \subseteq X \times Y$ with challenge space $C$. We say that $(P,V)$ is \textit{special honest verifier zero knowledge} (special HVZK), if there exists an efficient probabilistic algorithm \textbf{Sim} (called a simulator) that takes as input $(y,c) \in Y \times C$, and satisfies the following properties:
\begin{itemize}
    \item for all inputs $(y,c) \in Y \times C$, algorithm \textbf{Sim} always outputs a pair $(t,s)$ such that $(t,c,s)$ is an accepting conversation for $y$;
    \item for all $(x,y) \in R$, if we compute $c \chooserandom C, (t,s) \chooserandom \textbf{Sim}(y,c)$, then $(t,c,s)$ has the same distribution as that of a transcript of a conversation between $P(x,y)$ and $V(y)$.
\end{itemize}
Note that special HVZK implies HVZK \cite{boneh2017graduate}.

\paragraph{Attack game for one-way key generation}
\label{game:one-way}
Let $K$ be a key generation for $R \subseteq X \times Y$. For a given adversary $A$, the attack game runs as follows:
\begin{itemize}
    \item The challenger runs $K$ and gets $sk, pk$. It then sends $pk = y$ to $A$;
    \item $A$ outputs $\hat{x} \in X$. We say that the adversary wins the game if $(\hat{x}, y) \in R$.
\end{itemize}
We define $A$’s advantage with respect to $K$, denoted $\mathit{OWadv}[A,K]$, as the probability that $A$ wins the game. We say that a key generation algorithm $K$ is \textit{one way} if for all efficient adversaries $A$, the quantity $\mathit{OWadv}[A,K]$ is negligible.


\subsection{Identification protocol and security notions}

\paragraph{Identification protocols}
An \textit{identification protocol} $I = (K, P, V)$ is executed between a \role{Prover} $P$ and a \role{Verifier} $V$. A secret key $sk$ and its respective public key $pk$ are generated by $K$; its only argument (which we will omit for brevity from here on) is the security parameter $1^k$ to determine the key length. After generating the keys, $P$ receives the secret key $sk$ while both $P$ and $V$ receive the public key $pk$. Then a protocol is executed in which $P$ tries to convince $V$ it indeed holds the secret key $sk$.

\paragraph{Canonical identification protocol}
We denote an identification protocol \textit{canonical}, when it is executed as a characteristic three-move protocol. Denoted as $I = (K, P_t, V_c, P_s, V_v)$, after executing key generation $K$, it goes as follows: Commitment $t$ is generated by $P$ with $P_t(sk)$ and sent to $V$. $V$ responds with challenge $c$ generated by $V_c(pk, t)$. $P$ then calculates the receipt $s$ by $P_s(sk, t, c)$ and sends it to $V$. $V$ finally executes $V_v(pk, t, c, s)$ to \textit{accept} or \textit{reject} the execution. Note that all Sigma protocols are canonical.

\paragraph{Attack game for secure identification under direct attacks}
\label{game:imp-aa}
For a given identification protocol $I = (K,P,V)$ and a given adversary $A$, the attack game runs as follows:
\begin{itemize}
    \item \textit{Key generation phase.} The challenger runs $K$ and sends the resulting $pk$ to $A$.
    \item \textit{Impersonation attempt.} The challenger and $A$ now interact, with the challenger following the verifier’s algorithm $V$ (with input $pk$), and with $A$ playing the role of a prover, but not necessarily following the prover’s algorithm $P$ (indeed, $A$ does not receive the secret key $sk$).
\end{itemize}

We say that the adversary wins the game if $V$ outputs \texttt{accept} at the end of the interaction. We define $A$’s advantage with respect to $I$, denoted $\mathit{ID1adv}[A,I]$, as the probability that $A$ wins the game. We say that an identification protocol $I$ is \textit{secure against direct attacks} if for all efficient adversaries $A$, the quantity $\mathit{ID1adv}[A,I]$ is negligible \cite{boneh2017graduate}.

\paragraph{Attack game for secure identification under eavesdropping attack}
\label{game:imp-pa}
For a given identification protocol $I = (K,P,V)$ and a given adversary $A$, the attack game runs as follows:
\begin{itemize}
    \item \textit{Key generation phase.} The challenger runs $K$ and sends the resulting $pk$ to $A$.
    \item \textit{Eavesdropping phase.} The adversary requests some number, say $q$, of transcripts of conversations between $P$ and $V$. The challenger complies by running the interaction between $P$ and $V$ a total of $q$ times, each time with $P$ initialized with input $sk$ and $V$ initialized with $pk$. The challenger sends these transcripts $T_1, \dots ,T_q$ to the adversary.
    \item \textit{Impersonation attempt.} The challenger and $A$ now interact, with the challenger following the verifier’s algorithm $V$ (with input $pk$), and with $A$ playing the role of a prover, but not necessarily following the prover’s algorithm $P$ (indeed, $A$ does not receive the secret key $sk$).
\end{itemize}

We say that the adversary wins the game if $V$ outputs \texttt{accept} at the end of the interaction. We define $A$’s advantage with respect to $I$, denoted $\mathit{ID2adv}[A,I]$, as the probability that $A$ wins the game. We say that an identification protocol $I$ is \textit{secure against eavesdropping attacks} if for all efficient adversaries $A$, the quantity $\mathit{ID2adv}[A,I]$ is negligible \cite{boneh2017graduate}.

\section{Protocol}

Given our setting, we want the identification protocol to have a small secret size (optimally equal the security parameter) and need it to support key aggregation. Not as important is the element representation size or performance (as long as they remain within "realistic bounds" which we analyse in \cite{parameters}.)

\paragraph{Core idea} We implement this requirement by choosing a large random value $r$ plus a short secret $x$ in the exponent. We get security against the DL-breaking algorithms (due to the suitably large random $r$), while at the same time keeping our secret $x$ as short as our security parameter allows us to.

\paragraph{Public key} We cannot simply publish $r$ as part of the public key: Given $(r, g^{r+x})$, we can easily extract $g^x$, hence would not improve security against the DL-breaking algorithms. The same happens if we publish $g^r$, as again we can extract $g^x = \frac{g^{x + r}}{g^r}$. Further, we cannot include $r$ into the private key, as this clearly breaks our requirement of small private keys. We instead propose $(g_1^r, g_2^{r + x})$ for $g_1$, $g_2$ as independent generators.

\paragraph{Pairing} The prover (knowing $x$) generates $(g_1^{r'}, g_2^{r' + x})$ for uniform random $r'$. The verifier then validates using the pairing $\theta$ that $\theta(\frac{g_1^r}{g_1^{r'}}, {g_2}) = \theta({g_1}, \frac{g_2^{r + x}}{g_2^{r' + x}})$. To ensure the prover indeed knows $r'$ and $r' + x$ we do a zero-knowledge proof. \footnote{Else trivial solutions allow the Prover to pass validation, like replaying the public key.}

\subsection{Construction}

\paragraph{KeyGen() $\rightarrow (x, \hat{x})$}
\begin{nicodemus}[0]
    \item $x \chooserandom \mathbb{Z}_{2^k}$
    \medskip
    \item $r_1 \chooserandom \mathbb{Z}_p$
    \item $r_2  = r_1 + x$
    \medskip
    \item $\hat{x_1} = {g_1}^{r_1}$
    \item $\hat{x_2} = {g_2}^{r_2}$
    \medskip
    \item return $x$ and $\hat{x} = (\hat{x_1}, \hat{x_2})$.
\end{nicodemus}

\begin{protocol}{Pairing-based identification protocol.}
    \label{protocol:the_protocol_as_identification}

    Publicly known is the cyclic group $G$ (in which DL is believed to be hard) of order $p$ and its independent generators $g_1 \in G$ and $g_2 \in G$, the bilinear pairing $\theta: G \times G \rightarrow H$ (for some arbitrary $H$), and the maximal length $k$ of the private key, for $k \leq |p|$. \\


    \textbf{KeyGen} is used to generate the keys. The \role{Prover} $P$ receives the secret key $x$ and the \role{Verifier} $V$ receives the public key $\hat{x}$. Then the protocol is executed follows: \\

    \begin{minipage}[t]{\textwidth}
        \procedureblock{}{
            \textbf{Prover} \> \> \> \textbf{Verifier} \\[0.1\baselineskip][\hline]
            \\[-0.5\baselineskip]
            r_1' \chooserandom \mathbb{Z}_p \\[-0.2\baselineskip]
            r_2' = r_1' + x \\
            \hat{x_1}' = g_1^{r_1'} \\[-0.2\baselineskip]
            \hat{x_2}' = g_2^{r_2'} \pclb
            \pcintertext[dotted]{}
            k_1 \chooserandom \mathbb{Z}_p \\[-0.2\baselineskip]
            k_2 \chooserandom \mathbb{Z}_p \\
            t_1 = g_1^{k_1} \\[-0.2\baselineskip]
            t_2 = g_2^{k_2} \\
            \> \sendmessagerightx[8em]{2}{\hat{x_1}', \hat{x_2}', t_1, t_2} \\
            \< \> c \chooserandom \mathbb{Z}_p \\
            \> \sendmessageleftx[8em]{2}{c} \\
            s_1 = k_1 + r_1' * c \hspace{3em} \\[-0.2\baselineskip]
            s_2 = k_2 + r_2' * c \\
            \> \sendmessagerightx[8em]{2}{s_1, s_2} \\
            \< \> g_1^{s_1} = t_1 * \hat{x_1}'^{c} \\[-0.2\baselineskip]
            \< \> g_2^{s_2} = t_2 * \hat{x_2}'^{c} \pclb
            \pcintertext[dotted]{}
            \< \> \theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'})}
    \end{minipage}
\end{protocol}


\subsection{Parameters}
\label{parameters}

\paragraph{Performance requirements}
For the purpose the protocol is defined for, the most important requirement is the short secret key length while computational effort is not of a big concern. Still, improving performance is beneficial: A faster \role{Prover} might improve usability (as the voting client of the voter reacts faster), while an improved \textbf{KeyGen} and \role{Verifier} might allow the system to support more voters.

\paragraph{Operations}
In \cref{protocol:the_protocol_as_identification}, the \role{Prover} needs two exponentiations for each of the two generators. The \role{Verifier} needs two exponentiations, one multiplication and one division for each generator. The pairing $\theta$ is used twice, specifically to check if $\theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'})$.

\paragraph{Type of pairing required}
Note that \cref{protocol:the_protocol_as_identification} never mixes numbers that involve $g_1$ with numbers that involve $g_2$: Assuming $g_1 \in G_1$ and $g_2 \in G_2$, we do not require a homomorphism between $G_1$ and $G_2$. Under these conditions, we are free to choose either a \textit{Type 1}, \textit{Type 2} or \textit{Type 3} pairing.

\paragraph{Choosing pairing}
There exists a \textit{Type 3} pairing with efficient group operations in $G_1$, $k^2$-less-efficient group operations in $G_2$ and an efficient $\theta$ for a wide flexibility of system parameters. We might also opt for a \textit{Type 1} pairing (with efficient group operations in $G_2$), but its performance degrades fast with higher security levels, and it is less flexible concerning system parameters \cite{galbraith2008pairings}

\paragraph{Choosing parameters}
We require the group size to be $2^{2k}$ for $k$ the security parameter. \cite{galbraith2008pairings} Hence for the 128 bits security that CHVote requires at its highest security level 3, we need to choose a key size of 256 bits.

\section{Security Analysis}

To proof the identification scheme secure against impersonation we follow a proof construction described in \cite{boneh2017graduate}.

\paragraph{Security against impersonation under active attacks} We first show that the protocol is a sigma protocol, the key generation algorithm $K$ is one-way, and knowledge soundness of two accepting conversions. It then follows that the protocol is secure against impersonation under active attacks.

\paragraph{Security against impersonation under passive attacks} Given the security against active attack, we additionally show the protocol to be special HVZK. \footnote{While special HVZK is not required for the proof construction used in this paper, it is a requirement for other proof constructions (like the OR-proof construction \cite{boneh2017graduate}).} Out of this follows HVZK, and in turn security against impersonation under passive attacks.

\subsection{Sigma protocol}

\begin{theorem}
    \label{theorem:sigma}
    \cref{protocol:the_protocol_as_identification} is a sigma protocol.
\end{theorem}

\begin{proof}
    The prover algorithm $P$ and verifier algorithm $V$ form the $(P, V)$ pair of the sigma protocol for the effective relation $R \subseteq X \times Y$, where
    \begin{equation}
        \begin{split}
            X = \mathbb{Z}_p \times \mathbb{Z}_{2^k}, Y = G \times G & \\
            R = \{ ((r_1, x), (\hat{x_1}, \hat{x_2})): g_1^{r_1} = \hat{x_1} \wedge g_2^{r_1 + x} = \hat{x_2} \wedge \theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'}) \} &
        \end{split}
    \end{equation}

    Note that $\theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'})$ due to the bilinearity of $\theta$:
    \begin{equation}
        \begin{split}
            \theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) & = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'}) \\
            \theta(g_1, g_2)^{r_1 - r_1'} & = \theta(g_1, g_2)^{r_2 - r_2'} \\
            \theta(g_1, g_2)^{r_1 - r_1'} & = \theta(g_1, g_2)^{r_1 + x - (r_1' + x)} \\
            \theta(g_1, g_2)^{r_1 - r_1'} & = \theta(g_1, g_2)^{r_1 - r_1'}
        \end{split}
    \end{equation}

    The reader may verify the rest of the Sigma definition is fulfilled, too.
\end{proof}


\subsection{IMP-AA identification protocol}

To prove \cref{protocol:the_protocol_as_identification} secure against impersonation under active attacks, we apply \cref{theorem:id_sgima_ks_ow_to_id_imp_aa}:

\begin{theorem}{19.14 from \cite{boneh2017graduate}}
    \label{theorem:id_sgima_ks_ow_to_id_imp_aa}
    Let $(P,V)$ be a Sigma protocol for an effective relation $R$ with a large challenge space. Let $K$ be a key generation algorithm for $R$. If $(P,V)$ provides knowledge soundness and $K$ is one-way, then the identification scheme $I = (K,P,V)$ is secure against direct attacks. \cite{boneh2017graduate} \\

    \begin{minipage}{\theoremcorewidth}
        In particular, suppose $A$ is an efficient impersonation adversary attacking $I$ via a direct attack (see \cref{game:imp-aa}),  with  advantage $\epsilon = \mathit{ID1adv}[A,I]$. Then there  exists an efficient adversary $B$ attacking $G$ via $K$ (see \cref{game:one-way}) (whose running time is about twice that of $A$), with advantage $\epsilon' = \mathit{OWadv}[B,K]$, such that
        \begin{equation}
            \epsilon' \geq \epsilon^2 - \epsilon / N,
        \end{equation}
        where $N$ is the size of the challenge space, which implies
        \begin{equation}
            \epsilon \leq \frac{1}{N} + \sqrt{\epsilon'}
        \end{equation}
    \end{minipage}
\end{theorem}

\clearpage

\paragraph{Knowledge soundness}
Given two accepting conversations $(t, c, s)$ and $(t, c', s')$ (with $c \neq c'$) for $(\hat{x_1}, \hat{x_2}) \in Y$, \textbf{Ext} for \cref{protocol:the_protocol_as_identification} can output $(r_1, x) \in X$ such that $((r_1, x), (\hat{x_1}, \hat{x_2})) \in R$ as follows:

\begin{equation}
    \begin{split}
        r_1' & = \frac{s_1 - s_1'}{c - c'} = \frac{k_1 + r_1'*c - k_1 - r_1'*c'}{c - c'} = \frac{(c - c')*r_1'}{c - c'} \\
        r_2' & = \frac{s_2 - s_2'}{c - c'} = \frac{k_2 + r_2'*c - k_2 - r_2'*c'}{c - c'} = \frac{(c - c')*r_2'}{c - c'} \\
        \text{output } (r_1 &= r_1', x = r_2' - r_1')
    \end{split}
\end{equation}

\paragraph{One-way KeyGen}
Note that \textbf{KeyGen} of \cref{protocol:the_protocol_as_identification} computes $sk = x \chooserandom \mathbb{Z}_{2^k}$ and $pk = (\hat{x_1} = {g_1}^{r_1}, \hat{x_2} = {g_2}^{r_2})$ for $r_1 \chooserandom \mathbb{Z}_p$ and $r_2  = r_1 + x$. It is obvious that the computation of $pk$ out of $sk$ is one-way under the DL assumption.

\begin{theorem}
    \label{theorem:id_imp_aa}
    Under the DL assumption for $\mathbb{G}$, and assuming $N = |\mathbb{Z}_p|$ is super-poly, \cref{protocol:the_protocol_as_identification} is secure against active attacks.
\end{theorem}

\begin{proof}
    We have shown in \cref{theorem:sigma} that \cref{protocol:the_protocol_as_identification} is a Sigma protocol. Together with our proofs of knowledge soundness and of the one-way \textbf{KeyGen}, we have shown all preconditions of \cref{theorem:id_sgima_ks_ow_to_id_imp_aa}, and our claim follows.
\end{proof}

\subsection{IMP-PA identification protocol}

To prove our identification protocol secure against impersonation under passive attacks, we apply the following theorem:

\begin{theorem}{19.3 from \cite{boneh2017graduate}}
    \label{theorem:id_imp_aa_hvzk_to_id_imp_pa}
    If an identification protocol $I$ is secure against direct attacks, and is HVZK, then it is secure against eavesdropping attacks. \\

    \begin{minipage}{\theoremcorewidth}
        In particular, if $I$ is HVZK with simulator \textbf{Sim}, then for every impersonation adversary $A$ that attacks $I$ via an eavesdropping attack (see \cref{game:imp-pa}) obtaining up to $q$ transcripts, there is an adversary $B$ that attacks $I$ via a direct attack (see \cref{game:imp-aa}) where $B$ is an elementary wrapper around $A$ (and where $B$ runs \textbf{Sim} at most $Q$ times), such that
        \begin{equation}
            \mathit{ID2adv}[A,I] = \mathit{ID1adv}[B,I]
        \end{equation}
    \end{minipage}
\end{theorem}

\paragraph{Special HVZK}
To show that \cref{protocol:the_protocol_as_identification} is special HVZK, we show that we are able to generate transcripts $t'$ of a successful protocol run without knowing the secret $x$ given the challenge $c \chooserandom C$. These generated transcripts are perfect indistinguishable from real transcripts as we can show that their probability distributions are exactly the same.

In our original protocol, the distribution of the values in a protocol run ($\hat{x_1}'$, $\hat{x_2}'$, $t_1$, $t_2$, $c$, $s_1$, $s_2$) are as follows:

\begin{itemize}
    \item $\hat{x_1}'$ is calculated by raising the uniform random $r_1$ to the generator $g_1$.
    \item $\hat{x_2}'$ is calculated by raising the uniform random $r_1$ plus a secret $x$ to the generator $g_2$.
    \item $t_1$ is calculated by raising the uniform random $k_1$ to the generator $g_1$. Same for $t_2$, for the uniform random $k_2$ and the generator $g_2$.
    \item $s_1$ is calculated by adding the uniform random $k_1$ to another value. \footnote{Note that $k_1$ was already used to calculate $t_1$. This however does not produce any useful insight for the attacker, as these calculations are performed in the group $\mathbb{G}$ in which the hardness of the discrete log is assumed.}. Same for $s_2$, by adding the uniform random $k_2$.
\end{itemize}

We summarize that all values except $\hat{x_2}'$ are distributed uniformly at random: Either adding a uniform random or raising a uniform random to a generator leads to a uniform random distribution.

We avoid describing the distribution of $\hat{x_2}'$ exactly, but rather observe the following: The distribution of $\hat{x_2}'$ relates to the distribution of $\hat{x_1}'$ the same way as the distribution of the two values of the respective public key ($\hat{x_2}$, $\hat{x_1}$) relate to each other. Note that this is due to the fact as \textbf{KeyGen} generates $\hat{x_1}$ and $\hat{x_2}$ in the same way as the \role{Prover} generates $\hat{x_1'}$ and $\hat{x_2}$.

To generate a transcript without knowing $x$, replicating the observed distributions of a real transcript given the challenge $c \chooserandom C$, we propose to choose the values as follows:

\begin{nicodemus}[0]
    \item $r', s_1, s_2 \chooserandom \mathbb{Z}_p$
    \medskip
    \item $\hat{x_1}' = \hat{x_1} * g_1^{r'}$
    \item $\hat{x_2}' = \hat{x_2} * g_2^{r'}$
    \medskip
    \item $t_1  = g_1^{s_1} * \hat{x_1}'^{-c}$
    \item $t_2  = g_2^{s_2} * \hat{x_2}'^{-c}$
\end{nicodemus}

\leavevmode \\
We argue that indeed the distribution is identical to a real protocol run:

\begin{itemize}
    \item $\hat{x_1}'$ is distributed uniformly at random: As $r'$ is chosen uniformly at random, using it as an exponent for $g_1$ results in a uniform random value. Multiplying it to $\hat{x_1}$ "shifts" $\hat{x_1}$ by the uniform random amount, resulting in a uniform at random distribution.
    \item $\hat{x_2}'$ replicates the specific distribution relative to $\hat{x_1'}$ as observed in a real protocol run: We use the same uniform random $r'$ as an exponent for $g_2$ and multiply this to $\hat{x_2}$, hence preserving the relative distribution between $\hat{x_2}$ and $\hat{x_1}$ for $\hat{x_2}'$ and $\hat{x_1}'$.
    \item $t_1$ is distributed uniformly at random: As $s_1$ is chosen uniformly at random, using it as an exponent for $g_1$ results in a uniform random value. The second term $\hat{x_1}'^{-c}$ is also uniform random following the same argumentation. Multiplying two uniform random values results in a uniform at random distribution. The same argumentation applies to $t_2$, with $s_2$, $g_2$ and $\hat{x_1}'$.
    \item $c$, $s_1$ and $s_2$ are each distributed uniformly at random as each is chosen uniformly at random.
\end{itemize}

As a final step, we need to ensure the generated transcript also passes our correctness checks. There are three checks performed; two verifying the relationship between $t_1$, $c$ and $s_1$ (respectively $t_2$, $c$, $s_2$) and one involving our pairing:

\begin{equation}
    \begin{split}
        g_1^{s_1} = t_1 * \hat{x_1}'^{c} \hspace{2em} &= \hspace{2em} g_1^{s_1} = g_1^{s_1} * \hat{x_1}'^{-c} * \hat{x_1}'^{c} \\
        g_2^{s_2} = t_2 * \hat{x_2}'^{c} \hspace{2em} &= \hspace{2em} g_2^{s_2} = g_2^{s_2} * \hat{x_2}'^{-c} * \hat{x_2}'^{c}
    \end{split}
\end{equation}

\begin{equation}
    \begin{split}
        \theta(\frac{\hat{x_1}}{\hat{x_1}'}, {g_2}) & = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}'}) \\
        \theta(\frac{\hat{x_1}}{\hat{x_1}*g_1^{r'}}, {g_2}) & = \theta({g_1}, \frac{\hat{x_2}}{\hat{x_2}*g_2^{r'}}) \\
        \theta(g_1, g_2)^{-r'} & = \theta(g_1, g_2)^{-r'} \\
    \end{split}
\end{equation}

\paragraph{HVZK}
\cref{protocol:the_protocol_as_identification} is also HVZK, as this directly follows from special HVZK.

\begin{theorem}
    \label{theorem:id_imp_pa}
    Under the DL assumption for $\mathbb{G}$, and assuming $N = |\mathbb{Z}_p|$ is super-poly, \cref{protocol:the_protocol_as_identification} is secure against passive attacks.
\end{theorem}

\begin{proof}
    From \cref{theorem:id_imp_aa} we know \cref{protocol:the_protocol_as_identification} is secure against active attacks. Together with our HVZK proof, we fulfill all preconditions to apply \cref{theorem:id_imp_aa_hvzk_to_id_imp_pa} and our claim follows.
\end{proof}

\section{Conclusion}

\paragraph{Acknowledgments} We thank Julia Kaster, Dennis Hofheinz and Kenny Paterson for discussions and feedback concerning the proofs and implementation of the pairings.

% CHVote Team: Idea, Introduction, Definitions
% Florian Moser: Semester project, Proof
% Dennis & Julia: Supervision semester project, proof "pointing into the right direction", literature
% Kenny: Feedback semester project

% Future collaboration:
% - Dennis & Julia will read the draft. Ensure they look at the proofs specifically.
% - Kenny will read the draft too (MA supervisor Florian in similar area). Ensure he looks at the claims concerning the pairings.


%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{cryptobib/abbrev0,cryptobib_selection,references}

\end{document}
