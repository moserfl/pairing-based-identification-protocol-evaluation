# Contributing

## git-lfs

This project uses `git-lfs`. [Install](https://git-lfs.github.com/) it on your machine to be able to read the binary files (`.pdf` and similar).

## .fex

This project uses the `.fex` format for summaries (text file with indentation defining chapter association). The [FexCompiler](https://github.com/famoser/FexCompiler) can transform the text files to printing-optimized `.pdfs`. 