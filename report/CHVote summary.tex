\documentclass[]{article}

\newcommand{\chooserandom}{\xleftarrow{r}} % when an element is chosen randomly; like "x chosen randomly out of X"
\newcommand{\parametersize}{\scriptsize} % the size of the parameter description; like "m is message"
\newcommand{\role}{\textit} % the role of an agent; like "verifier"

\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm} % for bold math symbols
\usepackage{nicodemus}
\usepackage{famoser}

\usepackage{tikz}
\usetikzlibrary{matrix,shapes,arrows,positioning,chains, calc}

% to style quotes
\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\small}

\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	filecolor=magenta,
	urlcolor=cyan,
}


\title{CHVote: A concise summary}
\author{Florian Moser}

\begin{document}
	
	\maketitle


\section{CHVote Procotol}
% summary only to detect relevant factors of identification protocol

This is a summary of how the \textit{CHVote} \cite{haenni2017chvote} protocol works. 
It is not meant as basis for proving, implementing or verifying parts of the protocol; rather it is an overview aiming to help understand the overall concept of the scheme.

In particular, it is not meant to be published on its own. We therefore do not rigorously define all vocabulary and algorithms; the reader will find these (necessary and important) details in \cite{haenni2017chvote}. We further skip the description of two cryptographic primitives used in the protocol - the OT Scheme and the Shuffle Proof - as they have small overlaps with the personal interest of the author (which is the application of the identification scheme). Lastly, we skip the write-ins extension of the protocol (allows arbitrary candidates).

Still, this brief summary helps to give an overview of the protocol. With a solid background in information security, in particular some knowledge about signature schemes, one should be able to follow.

\section{Election context}
% protocol mapped to switzerlands system

The system is build to support direct democracy as implemented and practiced in Switzerland. Multiple elections may be held at the same time; there might be issues to be voted upon on federal, cantonal, municipal and pastoral level. 

Individual voters may only be able to take part in a subset of these elections (depending on their place of residence and citizen status). Once a vote as been submitted, the voter is not allowed to update it.

Responsible for organisation of the elections are the cantons. Voters are assigned to a counting circle of their municipality. While smaller municipalities may only consist of a single one, bigger municipalities may consist of several of these counting cycles. Results are published for each counting cycle. 

\paragraph{General election procedure}
% election procedure without references to switzerland

Voters receive a voting card with a finalization, abstention, voting and confirmation code. Further, it contains unique verification codes for each possible voting choice.

The process for the voter $V$, communicating with the voting system $S$, goes as follows:
\begin{itemize}
	\item $V$ submits its voting choice(s) and the voting code.
	\item $S$ validates the voting code, then returns a verification code for each voting choice.
	\item $V$ validates the verification codes, then submits the confirmation code.
	\item $S$ validates the confirmation code, then returns the finalization code.
	\item $V$ validates the finalization code.
\end{itemize}

Voters can only attempt the process once. If any step fails, voters are to abort and use a different voting channel. If the last step fails, they are instructed to call the election hotline to start an investigation. After the election period, voters check if either their abstention or the finalization code is published by the election authorities; depending on if they participated in the election or not.

Elections are \textit{k-out-of-n elections}: The voter chooses exactly $k$ out of  $n$ candidates, for candidates being a general term for voting choices. Many voting procedures can be mapped to this simple model, by introducing blank candidates (to select less than k candidates), abstention candidates (to abstain of some part of the vote) or duplicate candidates (to vote for a single candidate multiple times). The protocol requires that $0 < k < n$ but this constraint is of no practical concern in Switzerland. Multiple elections may run in parallel and voters might be constrained in which elections they can take part in. 

\section{Cryptographic primitives}
% primitives needed to understand to be convinced the protocol is technically feasible

We summarize the cryptographic primitives the CHVote protocol relies upon.

\subsection{ElGamal encryption} 

The ElGamal encryption is proven to be IND-CPA under DDH assumption.

\begin{protocol}{The ElGamal Protocol}
	Publicly known is the cyclic group $G$ (in which DDH is believed to be hard) and one of its generators $g \in G$. \\
	
	Keys are generated with \textbf{KeyGen}; the \role{Encryptor} $E$ receives the secret key $sk$ and the \role{Decryptor(s)} $D$ the public key $pk$. $D$ draws $r \chooserandom G$ and then encrypts a message $m$ with \textbf{Enc} to get the ciphertext $c$. $D$ can decrypt $c$ with \textbf{Dec}. 
	
	\paragraph{KeyGen() $\rightarrow (sk, pk)$}
	\begin{nicodemus}[0]
		\item $sk \chooserandom G$
		\item $pk = g^{sk}$
	\end{nicodemus}
	
	\paragraph{Enc($r$, $pk$, $m$) $\rightarrow c$}
	\begin{nicodemus}[0]
		\item $c = (m * {pk}^r, g^r)$
	\end{nicodemus}
	
	\paragraph{Dec($sk$, $c$) $\rightarrow m$}
	\begin{nicodemus}[0]
		\item $(a, b) = c$
		\item $m =  a * b^{-sk}$
	\end{nicodemus}

	\leavevmode \\
	Correctness holds by $m = a * b^{-sk} = m * {pk}^r * g^{r * -sk} = m * g^{sk * r} * g^{r * -sk} = m$.
\end{protocol}

\paragraph{Properties:}
\begin{itemize}
	\item Homomorphy with respect to multiplication (component-wise multiplication of two cyphertexts results in the product of the two plaintexts). 
	\item Re-encryption (by multiplying the cyphertext with an encryption of the neutral element 1).
	\item Shared keys (by aggregating multiple public keys into new public key; decryption needs all shared keys)
	\item Efficient multi-recipient / multi-message encryption (reused same random for multiple recipient or messages to reduce exponentiations required)
\end{itemize}

\subsection{Pederson commitment} 

The Pederson commitment is perfectly hiding and computationally binding under the DL assumption.

\begin{protocol}{The Pederson Commitment.}
	Publicly known is the cyclic group $G$ (in which DL is believed to be hard) and its independent generators $\{g, h_1, h_2, ...\} \subseteq G$.\\
	
	The \role{Commitor} $C$ draws $r \chooserandom G$ and then commits to a message $m = (m_1, m_2, ...)$ with \textbf{Com} and publishing $c$. The \role{Verifier} $C$ can open the commitment as soon as $C$ publishes both $m$ and $r$ with \textbf{Vfy}. 
	
	\paragraph{Com($m$, $r$) $\rightarrow c$}
	\begin{nicodemus}[0]
		\item $c = g^r * h_1^{m_1} * h_2^{m_2} * ...$
	\end{nicodemus}
	
	\paragraph{Vfy($c$, $m$, $r$) $\rightarrow$ $\mathtt{accept}$ or $\mathtt{reject}$}
	\begin{nicodemus}[0]
		\item if \textbf{Com($m$, $r$)} equals $c$ then $\mathtt{accept}$ else $\mathtt{reject}$
	\end{nicodemus}
\end{protocol}

\paragraph{Commitment to permutations} 
Rewrite a permutation $\psi : \{1, ..., n\} \rightarrow \{1, ..., n\}$ as a $n * n$ matrix, with entry $b_{ij}$ being 1 iff $\psi(i) = j$ else 0. We now commit individually to each column (resulting in $c_j = g^{r_j} * h_i$ for $i = \psi^{-1}(j)$) to get a commitment of size $O(n)$ of the whole permutation.

\subsection{Oblivious Transfer (OT)} 
Oblivious transfer allows a receiver to learn exactly k-out-of-n messages, without the sender knowing which k. \textit{That details of the OT-Scheme are intentionally omitted.}

\paragraph{OT-Scheme by Chu / Tzeng} 
Compared to general constructions (like constructing an k-out-of-n scheme out of 1-out-of-2 scheme) the  k-out-of-n OT-Scheme by Chu / Tzeng is more efficient.

It is stated about the assumptions \& properties of the OT-Scheme that:

\begin{quote}
In the random oracle model, the scheme is provably secure against a malicious receiver and a semi-honest sender. Receiver privacy is unconditional and weak sender privacy is computational under the chosen-target computational Diffie-Hellman (CT-CDH) assumption
\end{quote}

\paragraph{Full Sender Privacy extension to the OT-Scheme by Chu / Tzeng}
It is argued that in the eVoting context full sender privacy is required, so an extension to the OT-Scheme is proposed. The most important changes include that queries now correspond to ElGamal encryptions, receiver privacy is no longer unconditional (rather depends on the DDH) and it is less efficient than the original scheme.

\paragraph{Simultaneous OT}
Rather than executing multiple runs of the OT-scheme in parallel, it is proposed to instead merge the $m$ lists of $n$ values into a single run. A constraint is proposed to preserve the k-out-of-n selection out of the original lists. 

\paragraph{Arbitrary size hash construction}
The proposed OT-Scheme needs to XOR clear-text messages with hash values. As the message might be longer than the output of the hash function used in a concrete implementation, a construction is proposed to generate a hash value of arbitrary length.

A concatenated hash is produced by continuously appending a hash salted by increasing numbers starting at one (like $h(value, 1) || h(value, 2) || ...$) until the hash is of longer or equal length to the message. If it is longer than the message, bits from the concatenated hash are truncated from the start until the length to the message matches exactly.

\subsection{Non-interactive Preimage Proofs}
With a proof of knowledge, some \role{Prover} $P$ proves knowledge of some secret $x$ to a \role{Validator} $V$. These proofs are zero-knowledge if $V$ does not learn anything about the secret $x$ in the process.

\begin{protocol}{Three-move Zero-Knowledge Proof of Knowledge using a one-way group homomorphism.}
	Publicly known is the the one-way group homomorphism $\phi: X \rightarrow Y$ (in which inversion is believed to be hard). Note that a potential instantiation could use a cyclic group $G$ (in which DL is believed to be hard) and one of its generators $g \in G$ to define $\phi$ as $\phi(x): g^x = y$. \\
	
	The \role{Prover} $P$ creates a $t$ using a random value k\footnote{Note that $k$ is called $w$ in \cite{haenni2017chvote}.} and $\phi$ and sends it to the \role{Verifier} $V$. $V$ responds with a challenge $c$. $P$ uses the secret $x$ to generate a receipt $r$ based on $c$ and $t$. $V$ checks if the received $r$ indeed satisfies the correctness criteria. One interaction of this three-move protocol is fully defined by its triplet $(t, c, r)$.
	
	\begin{nicodemus}[0]
		\item P: $k \chooserandom X$
		\item P: $t = \phi(k)$
		\item P: sends $t$ to $V$
		\medskip
		\item V: $c \chooserandom X$
		\item V: sends $c$ to $P$
		\medskip
		\item P: $r = k + x*c$
		\item P: sends $r$ to $V$
		\medskip
		\item V: iff $\phi(r) = t * y^c$ then $\mathtt{accept}$ else $\mathtt{reject}$.
	\end{nicodemus}

	\leavevmode \\
	Correctness holds because $\phi(r) = t * y^c$ equals $\phi(k + x*c) = \phi(k) * \phi(x)^c$.
\end{protocol}

\paragraph{Fiat-Shamir heuristic}
With the Fiat-Shamir heuristic, this interactive proof can be converted to a non-interactive proof in the random oracle model: Instead of $V$ choosing the random $r$, $P$ calculates $c = h(y, t)$. The triplet $(t, c, r)$ can now be produced independently of $V$. For verification, $V$ needs to additionally check that indeed the expected $c$ was used.

This construction is used for the Schnorr Identification, proving knowledge of some private credential $x$.

\paragraph{AND-Compositions}
If multiple such proofs are needed at the same time, a combination into a \textit{AND-composition} is possible: $\bm{y} = (y_1, ..., y_n)$ and their corresponding $\bm{x} = (x_1, ..., x_n)$ are proved by $n$ $(t_i, c, r_i)$ triplets. All of these triplets relay on the same $c = h(\bm{y}, \bm{t})$, connecting all together.

This construction is used to simultaneously proof knowledge of plaintext $m$ and randomization $r$ of a given ElGamal cyphertext ($n=2$). Further, it can be used to proof that a specific private key has been used to decrypt some ElGamal cyphertext or that an ElGamal cyphertext contains some specific message $m$ (without revealing the randomization). 

\paragraph{OR-Compositions}
One can also proof knowledge of only a single secret $x_i$, without revealing which one. For this, transcripts are simulated for all $i \neq j$ (by picking random $s_i$ and $c_i$ and then calculating $t_i$). Then the $c_j = c - \sum_{i != j} c_i$ is determined, and for $j$ the proof proceeds as usual. $c$ is determined equivalently as for the AND-Composition.

This construction is used to proof that an ElGamal cyphertext contains one of several specific messages.

\paragraph{Combining AND- and OR-Compositions}
Combinations of AND- and OR-Compositions are straightforward. Such constructions can then be used to proof arbitrary statements in the CNF form.

\subsection{Wikström's Shuffle Proof}\label{shuffle}
One can perform a secret shuffle on a vector by choosing a random permutation, permuting and then computing re-encryptions of the values. To proof the shuffle did not alter any values, one proofs that the same values as before are contained in the final vector. 

\textit{That details of the Shuffle Proof are intentionally omitted.}

\subsection{Schnorr Signatures}
The Schnorr signature scheme is EUF-CMA secure in the random oracle model under the DL assumption.

\begin{protocol}{Schnorr Signature Scheme.}	
	Publicly known is the cyclic group $G$ (in which DDH is believed to be hard) and one of its generators $g \in G$. \\
	
	Keys are generated with \textbf{KeyGen}; the \role{Prover} $P$ receives the secret key $sk$ and both $P$ and the \role{Verifier} $V$ receive the public key $pk$. $P$ draws $r \chooserandom G$ and then signs a message $m$ with \textbf{Sign} to get the signature $\sigma$. $V$ can verify $\sigma$ with \textbf{Vfy}. 
	
	\paragraph{KeyGen() $\rightarrow (sk, pk)$}
	\begin{nicodemus}[0]
		\item $sk \chooserandom G$
		\item $pk = g^{sk}$
		\item returns private key $sk$ and public key $pk$.
	\end{nicodemus}
	
	\paragraph{Sign($pk$, $sk$, $m$, $r$) $\rightarrow \sigma$}
	\begin{nicodemus}[0]
		\item $r \chooserandom G$
		\item $c = h(pk, m, g^r)$
		\item $s = r - c*sk$
		\item returns signature $\sigma = (c, s)$.
	\end{nicodemus}
	
	\paragraph{Vfy($pk$, $\sigma$, $m$) $\rightarrow$ $\mathtt{accept}$ or $\mathtt{reject}$}
	\begin{nicodemus}[0]
		\item $(c, s) = \sigma$
		\item $c' = h(pk, m, pk^c * g^s)$
		\item iff $c == c'$ then $\mathtt{accept}$ else $\mathtt{reject}$
	\end{nicodemus}

	\leavevmode \\
	Correctness holds because $pk^c * g^s = g^{sk * c} * g^{r - c*sk} = g^r$.
\end{protocol}

\subsection{Hybrid Encryption and Key-Encapsulation}
Hybrid encryption combines the benefits of asymmetric encryption (public / private key) with those of symmetric encryption (performance, even with larger messages). With a \textit{key-encapsulation mechanism} (KEM), using asymmetric encryption, a key $K$ for symmetric encryption is derived. $K$ is then used in a second step to actually encrypt / decrypt the message in question. Secure constructions are possible depending on the underlying schemes.

Let KEM encapsulate a key $K$ with $(ek, K) = (g^r, h({pk}^r))$, for public key $pk$, some random $r$ and some hash function $h$. Using $ek$, a receiver that knows $sk$ for $g^{sk} = pk$ can then decapsulate $K = h({ek}^{sk})$. 

\section{Protocol Design}
The protocol knows \textit{election administrators} (set up the election), \textit{election authorities} (guarantee integrity and privacy of submitted votes), a \textit{printing authority} (prints \& delivers voting cards) and \textit{voters} using \textit{voting clients}. Secure connections exist from the \textit{election authority} to the \textit{printing authority}, from the \textit{printing authority} to the \textit{voter}, and bidirectional between \textit{voter} and \textit{voting client}.

\subsection{Adversary Model and Trust Assumptions}
A covert adversary trying to break integrity or secrecy of the votes is assumed. All parties are computationally bounded. At least one \textit{election authority} follows the protocol honestly.

Trust is assumed in the \textit{voting client} for privacy and in the \textit{printing authority} for privacy and integrity.

\subsection{System parameters}

There are parameters of two categories defined: 
Security parameters determine the hardness of the system and are based on key length recommendations from well-known organizations. 
Election parameters capture the particularities of each election, such as the number of voters or candidates.

\paragraph{Security parameters}
Defined is \textit{minimal privacy $\sigma$} (length of key to bruteforce to break privacy), \textit{minimal integrity $\tau$} (length of key to bruteforce to break integrity), \textit{deterrence factor $\epsilon$} (lower bound probability that a cheating attempt is detected) and \textit{number of election authorities $s$}.

Based on $\sigma$, $\tau$ and $\epsilon$, the rest is derived:
\begin{itemize}
	\item hash length for all used hashes is determined to be 2 * max($\sigma, \tau$) (the doubling accounts for birthday attacks\footnote{Birthday attacks exploit the mathematics behind the birthday problem. The birthday problem investigates the probability that two persons out of a set of n randomly chosen people have their birthday on the same day. This probability seems unintuitively high: For 23 people it is already over 50\%.}).
	\item prime length defining the encryption group $G_q$ is set relative to $\tau$ (according to NIST recommendations).
	\item prime length defining the identification group $G_{\hat{q}}$ and the prime field $G_{p'}$ used for polynomial interpolation is set relative to $\sigma$ (again, according to NIST recommendations).
	\item private credential length used for the identification (resulting in the voting and confirmation codes) is upper bounded by the identification group size and lower bounded by $\tau$ * 2  (again, the doubling accounts for birthday attacks).
\end{itemize}

The length of finalization and abstention codes is derived out of $\epsilon$. For a deterrence factor of $\epsilon = 0.999$, a length of at least 14 bits is required. Stored as bytes, then represented using an alphabet of 6 bits, it will require 3 letters of said alphabet. 

The length of verification codes is determined similarly, after archiving an additional property: To improve usability, multiple verification codes of the same value are prevented by prepending the code with a unique candidate number. Assuming the number of candidates is lower or equal to 1678, a length of at least 24 bits is required. Using the same transformation as for the other codes, this will require 4 letters of said alphabet.

\paragraph{Election parameters}

The \textit{election parameters} are represented in their most compact form as $EP = (U, c, d, e, n, k, E, w)$. The unique election identifier $U$ is included in every digital signature to separate fresh values from old ones of previous elections. 
$c = (C_1, ..., C_n)$ are the candidate descriptions and $d = (D_1, ..., D_{N_e})$ the voter descriptions; both impose no constraints on their contained data.
The protocol allows for $t$ concurrent k-out-of-n elections, with election descriptions in $e = (E_1, ..., E_t)$ (arbitrary data), their respective number of candidates in $n = (n_1, ..., n_t)$ and number of selections in $k = (k_1, ..., k_n)$.
Voter eligibility is determined in the matrix $E = (eij)_{N_E \times t}$, containing a 1 iff the voter $i$ is eligible in election $j$ and else a 0. 
Further, each voter is assigned to some counting cycle with $w = (w_1, ..., w_{N_E})$.

Complete voter-specific \textit{voter parameters} are ${VP}_i = (U, c, D_i, e, n, k, e_i, w_i)$.

\section{Technical Preliminaries}

\paragraph{Encoding of Votes and counting cycles}

For efficiency reasons, it is argued one should incorporate all votes into a single encryption. By representing each candidate as its own prime, one can define an invertible mapping: Combinations are multiplied together to a single value, with inversion through factorization. Similarly, additional information (like counting cycle in the CHVote case) can be incorporated into this single value. 

It is proposed to use the smallest primes within the encryption group for this purpose (except the smallest one which is already in use to protect vote privacy, see next paragraph). It is defined that the first $n$ such primes are used for the candidates and the following $w$ are used for the counting cycles.

\phantomsection
\label{protect_vote_privacy}
\paragraph{Protecting Vote Privacy of Voters with Restricted Eligibility}
By construction, some voters may be eligible to less or more elections than others, leading to smaller anonymity sets. To prevent this jeopardizing vote privacy, it is proposed to simulate participation for voters in all elections they are not eligible for at the granularity of counting cycles\footnote{If at least one voter by counting cycle is eligible for a specific election, for all voters which are not eligible, participation is simulated}.

To simulate participation, it is proposed to add the first k candidates of each simulated election to the vote. Simultaneously, votes with the same selection are added to the ballots by the election authorities, marked with the lowest prime of the encryption group. After the vote decryption these default votes are detected due to the mark and are subtracted from the final result.

\paragraph{Linking OT Queries to ElGamal Encryptions}
OT Queries and ElGamal encryptions can be linked together, using a transformation that replaces one of the generators used in the OT-Scheme with the public encryption key of ElGamal. As the OT-Query and the encrypted vote are now represented by the same numbers simultaneously, it is guaranteed that they contain exactly the same selection of candidates. \textit{We skip the exact details why and how this works, as the exact maths behind the OT-Scheme are out of scope.}

\phantomsection
\label{election_authority_polynomial_generation}
\paragraph{Verification Codes}
The verification codes guarantee the voter the ballot has been cast as intended at the server. At the same time, the way these are constructed, allow the vote to be checked for validity.

Each election authority picks a random polynomial of degree $k-1$, for $k$ the number of candidates the voter needs to select over all elections the voter is eligible to\footnote{Note that a polynomial with degree $k-1$ is uniquely defined by $k$ points.}. The authority then evaluates this polynomial at $n$ random points, resulting in a vector $\bm{p} = (p1, ..., p_n)$. For non-eligible candidates $i$, $p_i$ is simply set to 0. 

The printing authority receives one of these vectors from each election authority and creates the verification codes: It first hashes each entry, then uses XORs to collapse all entries of the same column into a single value, and then prepends the candidate number (to avoid any matching verification codes).

During vote casting, the voting client receives via the OT-query $k$ points of each polynomial from the respective election authority. This enables the voting client to reconstruct the verification codes. Further, it can evaluate the reconstructed polynomials $p_i$ at $y_i = p_i(0)$, with the sum of all $y_i$ resulting in the \texttt{vote validity credential}.

This construction prevents votes with too many candidates, as only $k$ OT-queries are answered (hence no more than k points can be learned). It also prevents too few candidates (or candidates selected twice), as then less than $k$ unique points are returned and reconstruction of the polynomial is impossible.

\paragraph{Finalization and Abstention Codes}
The finalization code is derived from the same polynomials than the verification codes. Instead of hashing single entries, the printing authority hashes the whole vector $\bm{p}$ of each printing authority, and then uses XOR to collapse all of theses hashes into a single value.

The voting client can reconstruct this same finalization code because after confirming the votes, the election authorities respond with the randomization used in the OT-Scheme. This allows the voting client to unpack all transmitted values (not only $k$ as it was the case before confirming the votes). Having all of these values, it can now perform the same steps as the printing authority to calculate the alleged finalization code.

For the abstention code, each election authority chooses a random array of the required abstention code length. These arrays are then sent to the printing authority, which combines them using XOR into the final abstention code. This abstention codes are published publicly online (the voting client is not needed to reconstruct them).

\phantomsection
\label{election_authority_shared_private_credential_generation}
\paragraph{Voter Identification}
The voter identifies itself when submitting the ballot with the \textit{private voting credential} $x \in \mathbb{Z}_{\hat{q}}$ and when confirming its vote with the \textit{private confirmation credential}  $y \in \mathbb{Z}_{\hat{q}}$. 

Each code is generated as follows: Each election authority generates a ${sk}_i \chooserandom Z_{\hat{q}}$ and a corresponding ${pk}_i = g^{{sk}_i}$. All secret keys ${sk}_i$ are sent to the printing authority, which combines them into $sk = \prod {sk}_i$, the code for the voting card. All public keys ${pk}_i$ are broadcast to the other election authorities, each combining them to the $pk = \prod {pk}_i$. 

\subsection{Election result}
The result, after mixing and decrypting the $N$ votes, is represented by $\bm{u} = (u_1, ..., u_N)$ (determines if the vote is to be added or subtracted), $\bm{V} = (v_{ik})_{N \times n}$ (the vote itself) and $\bm{W} = (w_{ic})_{N \times w}$ (counting cycle the vote belongs to). The election result is therefore given by $ER = (U, c, e, n, k, u, V, M)$.

\section{Protocol description}

A high-level description of the complete CHVote protocol follows. It uses the already introduced cryptographic primitives and technical preliminaries.

\subsection{Pre-Election Phase}
The first phase involves all necessary tasks to set up an election. Afterwards, each voter is equipped with its voting card $VC_i = (i, X_i, Y_i, rc_i, FC_i, AC_i)$, with the voter index $i$, voting code $X_i$, confirmation code $Y_i$, verification codes $rc_i$, finalization code $FC_i$ and abstention code $AC_i$. Besides the state for each voter, all election authorities share a (voter-independent) public key $pk$ with each having a secret private key share $sk_i$. 

\paragraph{Election Preparation}
Each election authority generates for each voter a polynomial $p$ for the verification and finalization code. While $p(0)$ is the private key share of the vote validity credential, two additional private key shares are picked to construct the voting and the confirmation code. 

For each private key share, the public key share is computed and broadcast to all other election authorities. All election authority end up with the same public credentials (voting code, confirmation code, vote validity credential) of all voters.

\paragraph{Printing of Code Sheets}
The printing authority receives for each voter from each election authority:
\begin{itemize}
	\item polynomial evaluations of a polynomial $p$ to construct the verification codes $rc_i$ and finalization code $FC_i$.
	\item two private credential shares to combine into the voting code $X_i$ and confirmation code $Y_i$, respectively.
	\item a private share to combine into the abstention code $AC_i$.
\end{itemize}

Then it creates for each voter, using all these codes and the voter index, a voter-specific voting card.

\paragraph{Key Generation}
The election authorities establish a public key $pk$ between each other, with each authority having a share $sk_i$ of the secret key.

\subsection{Election Phase}
The start and end of the phase is given by the election period. How early and late votes are detected is not part of the protocol.

\paragraph{Candidate selection}
The voter selects its candidates on the voting client, resulting in the set $S = {s_1, ..., s_k}$ for $k$ eligible candidates.

\paragraph{Vote casting}
The voting client constructs an OT-query, using the $pk$ of the election authorities, with an entry for each selected candidate in $S$. Out of the plaintext candidates in the OT-Query and the voting code a proof of knowledge is constructed. As a third value, the voters public credential is sent to each election authority.

Each election authority checks if the ballot is valid and the voter has not submitted its vote before. If both conditions are fulfilled, the OT-query is answered. This allows the voting client to reconstruct the verification codes of the selected candidates.
 
\paragraph{Vote confirmation}
The voter ensures the verification codes displayed by the voting client are the same as contained on the voting card, and then enters the confirmation code. The voting client then constructs the confirmation; consisting of the public confirmation credential, the public vote validity credential, and a proof of knowledge of the two private equivalents.

This confirmation is submitted to each election authority, which respond with the OT-Scheme randomizations. This allows the voting client to reconstruct the finalization code. As a final step, the voter ensures the finalization code displayed by the voting client is the same as contained on the voting card.

\subsection{Post-Election Phase}
The confirmed ballots are mixed and decrypted through the coordinating election authorities. Note that all broadcast proofs can be checked by anyone, specifically by each election authority. An election authority can simply refuse to continue the process if foulplay is suspected.

\paragraph{Mixing}
To mix, an election authority takes the vector of encrypted votes $\tilde{e}$, performs a secret shuffle, and broadcasts both the result $\tilde{e}'$ and a proof of the secret shuffle. All election authorities repeat this process one after the other, each operating on the result $\tilde{e}'$ of its respective predecessor. It ends with the last election authority to shuffle producing $\tilde{e}' = \tilde{e}_{last}$.

\paragraph{Decryption}
To decrypt, an election authority uses its secret key share $sk_i$ to partially decrypt $\tilde{e}_{last}$, and broadcasts both the result $c_i$ and a proof that each entry has been decrypted using $sk_i$. All election authorities repeat this process one after the other, each operating on the result $c_i$ of its respective predecessor. It ends with the last election authority to decrypt producing $\tilde{c_i}' = \tilde{c}_{last}$.

\paragraph{Tallying}
The election administrator factorizes each decrypted vote of $\tilde{c}_{last}$ into the candidate selections per vote, and the marker whether the vote should be added or subtracted from the result. After summing up all votes appropriately, the election administrator publishes the result.

\paragraph{Inspection}
Election authorities publish their share of the finalization and abstention code to a public list. Voters can now verify that their finalization code (if they voted) or their abstention code (if they did not vote) appear on the list.


\bibliography{cryptobib/abbrev0,cryptobib/crypto,sources}
\bibliographystyle{alphaurl}
\end{document}
