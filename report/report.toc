\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {paragraph}{Contributions}{2}{section*.2}%
\contentsline {section}{\numberline {2}Cryptographic Primitives}{3}{section.2}%
\contentsline {paragraph}{Notation}{3}{section*.3}%
\contentsline {paragraph}{Terminology}{3}{section*.4}%
\contentsline {paragraph}{Groups}{3}{section*.5}%
\contentsline {paragraph}{Pairing}{3}{section*.6}%
\contentsline {subsection}{\numberline {2.1}Cryptographic basics}{4}{subsection.2.1}%
\contentsline {paragraph}{Negligible}{4}{section*.7}%
\contentsline {paragraph}{Security parameter}{4}{section*.8}%
\contentsline {paragraph}{Hardness Assumptions}{4}{section*.9}%
\contentsline {paragraph}{Solving the discrete log}{4}{section*.10}%
\contentsline {paragraph}{Hash functions}{4}{section*.11}%
\contentsline {subsection}{\numberline {2.2}Proofs of Knowledge}{4}{subsection.2.2}%
\contentsline {paragraph}{Non-interactive Proof of Knowledge in the Random Oracle Model}{5}{section*.12}%
\contentsline {paragraph}{Proof($G$, $g$, $y$, $x$) $\rightarrow \pi $}{6}{section*.13}%
\contentsline {paragraph}{Verify($G$, $g$, $y$, $\pi $) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{6}{section*.14}%
\contentsline {subsection}{\numberline {2.3}Sigma Protocols and Properties}{6}{subsection.2.3}%
\contentsline {paragraph}{Effective relation}{6}{section*.15}%
\contentsline {paragraph}{Sigma Protocol}{6}{section*.16}%
\contentsline {paragraph}{Knowledge soundness}{7}{section*.17}%
\contentsline {paragraph}{Honest Verifier Zero Knowledge}{7}{section*.18}%
\contentsline {paragraph}{Special Honest Verifier Zero Knowledge}{7}{section*.19}%
\contentsline {paragraph}{Attack game for one-way key generation}{7}{section*.20}%
\contentsline {subsection}{\numberline {2.4}Identification protocols}{8}{subsection.2.4}%
\contentsline {paragraph}{Canonical identification protocol}{8}{section*.21}%
\contentsline {paragraph}{Security notions}{8}{section*.22}%
\contentsline {paragraph}{Attack game for secure identification under direct attacks}{8}{section*.23}%
\contentsline {paragraph}{Attack game for secure identification under eavesdropping attack}{9}{section*.24}%
\contentsline {subsection}{\numberline {2.5}Signature schemes}{9}{subsection.2.5}%
\contentsline {paragraph}{Security notions}{9}{section*.25}%
\contentsline {paragraph}{Attack game for secure signature under chosen message attack (existential forgery)}{10}{section*.26}%
\contentsline {section}{\numberline {3}Authentication in \textit {CHVote}}{10}{section.3}%
\contentsline {paragraph}{Adversary model}{10}{section*.27}%
\contentsline {paragraph}{Hardness assumptions of \textit {CHVote}}{10}{section*.28}%
\contentsline {subsection}{\numberline {3.1}Authentication within \textit {CHVote}}{10}{subsection.3.1}%
\contentsline {paragraph}{Requirements of authentication}{10}{section*.29}%
\contentsline {paragraph}{Overview about authentication}{11}{section*.30}%
\contentsline {subsection}{\numberline {3.2}Authenticating messages}{11}{subsection.3.2}%
\contentsline {paragraph}{Security Guarantees}{11}{section*.33}%
\contentsline {paragraph}{Length of the secret}{11}{section*.34}%
\contentsline {subsection}{\numberline {3.3}Proposed non-interactive identification protocol}{11}{subsection.3.3}%
\contentsline {paragraph}{Length of the secret in the \textit {Protocol} }{11}{section*.38}%
\contentsline {paragraph}{Proof($pk$, $b$, $sk$, $a$) $\rightarrow \pi $}{12}{section*.31}%
\contentsline {paragraph}{Verify($pk$, $b$, $\pi $) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{12}{section*.32}%
\contentsline {paragraph}{Anticipated changes in \textit {CHVote} when implementing the \textit {Protocol}}{12}{section*.39}%
\contentsline {paragraph}{KeyGen() $\rightarrow (x, \hat {x})$}{13}{section*.35}%
\contentsline {paragraph}{Proof($x$) $\rightarrow (\pi _1, \pi _2, \hat {x}')$}{13}{section*.36}%
\contentsline {paragraph}{Verify($\hat {x}$, $\pi _1'$, $\pi _2'$, $\hat {x}'$) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{13}{section*.37}%
\contentsline {section}{\numberline {4}A pairing-based identification protocol}{14}{section.4}%
\contentsline {subsection}{\numberline {4.1}Sigma protocol}{14}{subsection.4.1}%
\contentsline {paragraph}{Theorem 1}{14}{section*.40}%
\contentsline {subsection}{\numberline {4.2}IMP-AA identification protocol}{14}{subsection.4.2}%
\contentsline {paragraph}{Theorem 2\hspace {1px} (19.14 from \cite {boneh2017graduate})}{14}{section*.41}%
\contentsline {paragraph}{Knowledge soundness}{16}{section*.42}%
\contentsline {paragraph}{One-way KeyGen}{16}{section*.43}%
\contentsline {paragraph}{Theorem 3}{16}{section*.44}%
\contentsline {subsection}{\numberline {4.3}IMP-PA identification protocol}{16}{subsection.4.3}%
\contentsline {paragraph}{Theorem 4\hspace {1px} (19.3 from \cite {boneh2017graduate})}{16}{section*.45}%
\contentsline {paragraph}{Special HVZK}{16}{section*.46}%
\contentsline {paragraph}{HVZK}{18}{section*.47}%
\contentsline {paragraph}{Theorem 5}{18}{section*.48}%
\contentsline {subsection}{\numberline {4.4}EUF-CMA signature scheme}{18}{subsection.4.4}%
\contentsline {paragraph}{Theorem 6\hspace {1px} (3.3 from \cite {EC:AABN02})}{18}{section*.51}%
\contentsline {paragraph}{Sign($sk$, $m$) $\rightarrow \pi $}{19}{section*.49}%
\contentsline {paragraph}{Verify($pk$, $m$, $\pi $) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{19}{section*.50}%
\contentsline {paragraph}{Theorem 7}{19}{section*.57}%
\contentsline {paragraph}{Sign($sk$, $m$) $\rightarrow \pi $}{20}{section*.52}%
\contentsline {paragraph}{Verify($pk$, $m$, $\pi $) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{20}{section*.53}%
\contentsline {subsection}{\numberline {4.5}Pairing implementation notes}{20}{subsection.4.5}%
\contentsline {paragraph}{KeyGen() $\rightarrow (x, \hat {x})$}{21}{section*.54}%
\contentsline {paragraph}{Sign($x$, $m$) $\rightarrow (\pi _1, \pi _2, \hat {x}')$}{21}{section*.55}%
\contentsline {paragraph}{Verify($\hat {x}$, $m$, $\pi _1$, $\pi _2$, $\hat {x}'$) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{21}{section*.56}%
\contentsline {section}{\numberline {5}Alternatives}{22}{section.5}%
\contentsline {paragraph}{Pseudo-Random Generator}{22}{section*.58}%
\contentsline {paragraph}{[Negative Result] Modified Schnorr Signature}{22}{section*.59}%
\contentsline {paragraph}{KeyGen() $\rightarrow (sk, pk)$}{22}{section*.60}%
\contentsline {paragraph}{Sign($sk$, $pk$, $m$) $\rightarrow \sigma $}{22}{section*.61}%
\contentsline {paragraph}{Verify($pk$, $\sigma $, $m$) $\rightarrow $ $\mathtt {accept}$ or $\mathtt {reject}$}{22}{section*.62}%
\contentsline {paragraph}{[Negative result] Replacing the pairing}{23}{section*.63}%
