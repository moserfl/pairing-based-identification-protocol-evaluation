# Pairing-Based Identification Protocol Evaluation

We analyse a Pairing-Based Identification Protocol (in the following simply referred to as the *Protocol*) which could be useful to improve efficiency in an eVoting scheme. The *Protocol* is proposed in section "11.2.1 Approach 1: Using Bilinear Mappings" at https://eprint.iacr.org/2017/325.

We propose to proceed as follow:
- [x] Define the *Protocol* formally
- [x] Analyse the scheme the *Protocol* is intended to replace
- [x] Analyse efficiency of the *Protocol*
- [x] Formalize assumptions and properties of the *Protocol*
- [x] Prove the *Protocol*

The project is done as part of the [Research in Computer Science](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?semkez=2020W&lerneinheitId=141206) course. It is supervised by Dennis Hofheinz and Julia Kastner. Further, we are able to coordinate with the original authors of the scheme.

## Submodules

This project uses submodules. Clone with `--recurse-submodules`.

To download the submodules lates (or if you have forgot the flag) do the following:
- `cd report\cryptobib`
- `git submodule init`
- `git submodule update`